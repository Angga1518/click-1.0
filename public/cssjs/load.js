window.addEventListener('load',function() {
    $("#loader").fadeOut();
    $("#textLoadingPage").css("animation", "none")
})

buttons = $('a')
buttons.each(function(i, but){
    but = $(but);
    var isiHref = but.attr('href');
    if (isiHref.includes("/") && but.attr('target')!='_blank') {
        $(but).click(function() {
            $('#loader').fadeIn();
        })
    }
})
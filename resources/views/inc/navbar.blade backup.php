<nav class="navbar navbar-expand-lg navbar-light navs1 pl-5 fixed-top" id="navId" style="background-color: ">
    <a class="navbar-brand" href="/">
      <svg xmlns="http://www.w3.org/2000/svg" class="logonav-size" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 547.13 472.71"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="673.08" y1="553.85" x2="70.08" y2="31.85" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ec791c"/><stop offset="1" stop-color="#f49a32"/></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M532.32,60.36a24,24,0,0,0-26.16,5.2L276.35,295.37a24,24,0,0,0,17,41h94.13A181.28,181.28,0,0,1,150.37,396C62.18,348.6,29.15,238.67,76.57,150.49A181.32,181.32,0,0,1,300.3,66.73a27.49,27.49,0,0,0,34-12.69h0a27.55,27.55,0,0,0-14.6-38.8C210.49-26.12,84.9,18.82,28.11,124.43-33.55,239.1,9.65,382.82,124.31,444.48c111.82,60.13,250.46,21,314.82-87l67,67a24,24,0,0,0,41-17v-325A24,24,0,0,0,532.32,60.36ZM499.13,349.59l-54.22-54.22a24,24,0,0,0-17-7H351.26L499.13,140.47Z"/></g></g></svg>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav ml-auto mr-5">
        <li class="{{ Request::is('catering','convection','laundry','logistics','souvenir') ? 'navActive nav-item dropdown' : 'nav-item dropdown' }} dropdown">
          <a class="dropdown-toggle hov" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Services
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/catering/">Catering</a>
            <a class="dropdown-item" href="/convection/">Convection</a>
            <a class="dropdown-item" href="/laundry/">Laundry</a>
            <a class="dropdown-item" href="/logistics/">Logistics</a>
            <a class="dropdown-item" href="/souvenir/">Souvenir</a>
          </div>
        </li>
        <li class="{{ Request::is('about') ? 'navActive nav-item' : 'nav-item' }}">
          <a href="/about/">About Us</a>
        </li>
        <li class="{{ Request::is('contact') ? 'navActive nav-item' : 'nav-item' }}">
          <a href="#footer">Contact</a>
        </li>
      </ul>
    </div>
  </nav>
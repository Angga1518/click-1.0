<div class="row descRow xsnap">
    <div class="col">
        <div class="card cardAboutUseful">
            <div class="cardTextAbout">
                <h2>Useful</h2>
                <p>CLICK akan terus membuat fitur dan layanan yang dapat <b>bermanfaat</b> bagi penyedia jasa dan Masyarakat.</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card cardAboutInnovative">
            <div class="cardTextAbout">
                <h2>Innovative</h2>
                <p>CLICK akan terus <b>berinovasi</b> untuk meningkatkan layanan kami dan memberikan kenyamanan untuk para pengguna.</p>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card cardAboutGrow">
            <div class="cardTextAbout">
                <h2>Grow Up</h2>
                <p>CLICK akan terus <b>berkembang</b> sehingga dapat memberikan dampak baik untuk cakupan yang lebih besar.</p>
            </div>
        </div>
    </div>
</div>

<div class="footer" id="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md">
                <div class="row">
                    <div class="col-lg-5 show-web">
                        <h1><b>CLICK</b></h1>
                        <p>CLICK merupakan on-demand services marketplace
                            berbasis aplikasi yang menghubungkan
                            penyedia jasa dengan konsumen.
                        </p>
                        <div style="height:30px"></div>
                    </div>
                    <div class="col">
                        <p><b>Sitemap</b></p>
                        <a href="{{ Request::is('home') ? '#service1' : '/home/#service1' }}"><p>Services</p></a>
                        <a href="{{ Request::is('about') ? '#top' : '/about' }}"><p>About Us</p></a>
                    </div>
                    <div class="col">
                        <p><b>About Us</b></p>
                        <a href="{{ Request::is('about') ? '#top' : '/about' }}"><p>What is CLICK?</p></a>
                        <a href="{{ Request::is('about') ? '#core' : '/about/#core' }}"><p>Core Values</p></a>
                        <a href="{{ Request::is('about') ? '#ourTeam' : '/about/#ourTeam' }}"><p>Meet the Team</p></a>
                    </div>
                    <div class="col">
                        <p><b>Services</b></p>
                        <a href="{{ Request::is('convection') ? '#top' : '/convection' }}"><p>Convection</p></a>
                        <a href="{{ Request::is('souvenir') ? '#top' : '/souvenir' }}"><p>Souvenir</p></a>
                        <a href="{{ Request::is('logistics') ? '#top' : '/logistics' }}"><p>Logistics</p></a>
                        <a href="{{ Request::is('laundry') ? '#top' : '/laundry' }}"><p>Laundry</p></a>
                        <a href="{{ Request::is('catering') ? '#top' : '/catering' }}"><p>Catering</p></a>
                    </div>
                </div>
            </div>
            <div class="vl show-web"></div>
            <div class="col-md">
                <div class="row">
                    <div class="col-lg">
                        <h1 class="show-web"><b>Want to know more?</b></h1>
                        <p>Find us on</p>
                        <div class="row">
                            {{-- <div class="sosmedImages"><a href=""><i class="fa fa-linkedin" style="font-size:30px"></i></a></div> --}}
                            <div class="sosmedImages"><a href="https://wa.me/+6287828993811?text=Hi%20Click!%20I%20want%20to%20ask%20something."><i class="fa fa-whatsapp" style="font-size:30px"></i></a></div>
                            <div class="sosmedImages"><a href="https://www.instagram.com/clickapps.id/?hl=en"><i class="fa fa-instagram" style="font-size:30px"></i></a></div>
                            {{-- <div class="sosmedImages"><a href=""><i class="fa fa-youtube" style="font-size:30px"></i></a></div> --}}
                        </div>
                        <div style="height:30px"></div>
                    </div>
                    <div class="col ptclick">
                        <p><b>CLICK Indonesia </b></p>
                        <div class="row">
                            <div class="col-1"><i class="fa fa-map-marker" style="font-size:24px"></i></div>
                            <div class="col"><p>Bogor, Indonesia</p></div>
                        </div>
                        <div class="row">
                            <div class="col-1"><i class="fa fa-phone" style="font-size:24px"></i></div>
                            <div class="col"><p>+62 878-2899-3811</p></div>
                        </div>
                        <div class="row">
                            <div class="col-1"><i class="fa fa-envelope" style="font-size:20px"></i></div>
                            <div class="col"><p>Clickstartup.id@gmail.com</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backcopcolor">
        <!-- Copyright -->
        <div class="footer-copyright text-center py-2">Copyright © 2020 Click</div>
        <!-- Copyright -->
    </div>
</div>
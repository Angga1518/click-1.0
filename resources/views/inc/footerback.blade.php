<!-- Footer -->
<footer class="footer">
    <div class="backlinkcolor">
        <!-- Footer Links -->
        <div class="footBox">
            <!-- Grid row -->
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <!-- Grid column -->
                        <div class="col-md-6 narasiFooter link">
                            <!-- Links -->
                            <h1 class="font-weight-bold text-uppercase mt-3">CLICK</h5>
                            <p>CLICK merupakan on-demand services marketplace berbasis aplikasi yang menghubungkan penyedia jasa dengan konsumen.
                            </p>
                        </div>

                        <!-- Grid column -->
                
                        <!-- Grid column -->
                        <div class="col link">
                            <!-- Links -->
                            <h5 class="font-weight-bold mt-4 mb-4">Sitemap</h5>
                            <ul class="list-unstyled">
                                <li><a href="{{ Request::is('home') ? '#aboutUs1' : '/home/#aboutUs1' }}">Our Philosophy</a></li>
                                <li><a href="{{ Request::is('home') ? '#service1' : '/home/#service1' }}">Services</a></li>
                                <li><a href="/contact">Contact Page</a></li>
                            </ul>
                        </div>
                        <!-- Grid column -->
                
                        <!-- Grid column -->
                        <div class="col link">
                            <!-- Links -->
                            <h5 class="font-weight-bold mt-4 mb-4">About Us</h5>
                            <ul class="list-unstyled">
                                <li><a href="/about">What is CLICK?</a></li>
                                <li><a href="{{ Request::is('about') ? '#core' : '/about/#core' }}">Core Values</a></li>
                                <li><a href="{{ Request::is('about') ? '#ourTeam' : '/about/#ourTeam' }}">Meet the Team</a></li>
                            </ul>
                        </div>
                        <!-- Grid column -->
                
                        <!-- Grid column -->
                        <div class="col link">
                            <!-- Links -->
                            <h5 class="font-weight-bold mt-4 mb-4">Services</h5>
                            <ul class="list-unstyled">
                                <li><a href="/convection">Convection</a></li>
                                <li><a href="/souvenir">Souvenir</a></li>
                                <li><a href="/logistics">Logistics</a></li>
                                <li><a href="/laundry">Laundry</a></li>
                                <li><a href="/catering">Catering</a></li>
                            </ul>
                        </div>
                        <!-- Grid column -->
                    </div>
                </div>
                <div class="vl"></div>
                <div class="col-md-5">

                    <div class="row">
                        <div class="col-md-8 mb-4 narasiFooterKanan link">
                            <!-- Links -->
                            <h1 class="font-weight-bold mt-3">Want to know more?</h5>
                            <p>Find us on</p>
                            <a class="mr-3" style="color: white" href="https://www.linkedin.com/">
                                <i class="fa fa-linkedin" style="font-size:24px"></i>
                            </a>
                            <a class="mr-3" style="color: white" href="https://www.instagram.com/">
                                <i class="fa fa-instagram" style="font-size:24px"></i>
                            </a>
                            <a class="mr-3" style="color: white" href="https://www.whatsapp.com/">
                                <i class="fa fa-whatsapp" style="font-size:24px"></i>
                            </a>
                            <a class="mr-3" style="color: white" href="https://www.telegram.org/">
                                <i class="fa fa-telegram" style="font-size:24px"></i>
                            </a>
                        </div>
                        <div class="col-md-4 link">
                            <!-- Links -->
                            <h5 class="font-weight-bold mt-4 mb-4">PT Click</h5>
                            <ul class="list-unstyled">
                                <li style="font-family: 'Barlow Condensed', sans-serif; color:white"><i class="fa fa-map-marker" style="font-size:15px; color:white;"></i> &nbsp Bogor, Indonesia   </li>
                                <li style="font-family: 'Barlow Condensed', sans-serif; color:white"><i class="fa fa-phone" style="font-size:15px; color:white;"></i> &nbsp 081399999999</li>
                                <li style="font-family: 'Barlow Condensed', sans-serif; color:white"><i class="fa fa-envelope" style="font-size:15px; color:white;"></i> &nbsp email@clickapps.id</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Grid row -->
        </div>
        <!-- Footer Links -->
    </div>
    <div class="backcopcolor">
        <!-- Copyright -->
        <div class="footer-copyright text-center py-2">Copyright © 2020 Click
        </div>
        <!-- Copyright -->
    </div>
  
  </footer>
  <!-- Footer -->
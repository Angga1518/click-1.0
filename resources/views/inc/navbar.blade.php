
<nav id="navId">
	
	<div class="navi" id="navId" style="height: inherit">
		<div class="hamburger">
			<img class="dropdownButton" src="{{ asset('asset/dropdown.svg') }}" height="15px" alt="Dropdown">
		</div>
		<div class="container" style="height: inherit;">
			<div class="row" style="height: inherit;">
				<div class="col">
					<a class="logo_pc" href="{{ Request::is('home') ? '#top' : '/' }}"><img height="40px" src="{{ asset('asset/LogoYellow.svg') }}" alt="CLICK"></a>
					<a class="logo_mobile" href="{{ Request::is('home') ? '#top' : '/' }}"><img src="{{ asset('asset/LogoYellow.svg') }}" height="30px" alt="CLICK"></a>
				</div>
				<div class="col" style="padding-right: 15px;">
					<ul class="nav-links">
						<li class="{{ Request::is('catering','convection','laundry','logistics','souvenir') ? 'navActive nav-item dropdown' : 'nav-item dropdown' }}">
							<a class="dropdown-toggle hov" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Services
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ Request::is('catering') ? '#top' : '/catering' }}">Katering</a>
								<a class="dropdown-item" href="{{ Request::is('convection') ? '#top' : '/convection' }}">Konveksi</a>
								<a class="dropdown-item" href="{{ Request::is('laundry') ? '#top' : '/laundry' }}">Laundry</a>
								<a class="dropdown-item" href="{{ Request::is('logistics') ? '#top' : '/logistics' }}">Logistik</a>
								<a class="dropdown-item" href="{{ Request::is('souvenir') ? '#top' : '/catering' }}">Souvenir</a>
							</div>
						</li>
						<li class="{{ Request::is('about') ? 'navActive nav-item' : 'nav-item' }}">
							<a class="hov" href="/about/">About Us</a>
						</li>
						<li class="{{ Request::is('contact') ? 'navActive nav-item' : 'nav-item' }}">
							<a class="hov" href="#footer">Contact</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Farizio was here -->
	</div>
</nav>
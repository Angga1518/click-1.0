<div class="col-4 show-web">
    <div class="container">
        @include('inc.servicesDesc')
    </div>
</div>
<div class="col">
    <div class="card">
        <a class="card-image" href="/convection/" target="_blank" style="background-image: url({{ asset('asset/Konveksi.svg') }});">
            <img src="{{ asset('asset/Konveksi.svg') }}" />
        </a>
        <a class="card-description" href="/convection/" target="_blank">
            <h2>Konveksi</h2>
            <p>Siap menyelesaikan pesanan Anda dengan kualitas sesuai keinginan Anda</p>
        </a>
    </div>
</div>
<div class="col">
    <div class="card">
        <a class="card-image" href="/souvenir/" target="_blank" style="background-image: url({{ asset('asset/Souvenir.svg') }});">
            <img src="{{ asset('asset/Souvenir.svg') }}" />
        </a>
        <a class="card-description" href="/souvenir/" target="_blank">
            <h2>Souvenir</h2>
            <p>Menyediakan berbagai macam oleh-oleh khas daerah dari berbagai daerah</p>
        </a>
    </div>
</div>
<div class="col">
    <div class="card">
        <a class="card-image" href="/logistics/" target="_blank" style="background-image: url({{ asset('asset/Logistic.svg') }});">
            <img src="{{ asset('asset/Logistic.svg') }}" />
        </a>
        <a class="card-description" href="/logistics/" target="_blank">
            <h2>Logistik</h2>
            <p>Mempertemukan Anda dengan pemilik barang yang Anda butuhkan</p>
        </a>
    </div>
</div>
<div class="col">
    <div class="card">
        <a class="card-image" href="/laundry/" target="_blank" style="background-image: url({{ asset('asset/Laundry.svg') }});">
            <img src="{{ asset('asset/Laundry.svg') }}" />
        </a>
        <a class="card-description" href="/laundry/" target="_blank">
            <h2>Laundry</h2>
            <p>Membantu Anda dalam mencuci pakaian di tengah kesibukan sehari-hari</p>
        </a>
    </div>
</div>
<div class="col">
    <div class="card">
        <a class="card-image" href="/catering/" target="_blank" style="background-image: url({{ asset('asset/Catering.svg') }});">
            <img src="{{ asset('asset/Catering.svg') }}" />
        </a>
        <a class="card-description" href="/catering/" target="_blank">
            <h2>Katering</h2>
            <p>Berbagai menu katering akan membantu Anda membuat acara yang hebat</p>
        </a>
    </div>
</div>

<div class="col serviceLastCard">
    <div class="serviceLastCard"></div>
</div>
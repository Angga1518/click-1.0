<div class="row descRow xsnap">
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/icha.png') }});">
                <img src="{{ asset('asset/person_pic/icha.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Aisyah Melleinia F.</h2>
                <p style="text-align: center">CEO</p>
            </a>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/tasya.png') }});">
                <img src="{{ asset('asset/person_pic/tasya.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Nastasya Larasati L.</h2>
                <p style="text-align: center">CFO</p>
            </a>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/zain.png') }});">
                <img src="{{ asset('asset/person_pic/zain.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Fathan Zain Alwafi</h2>
                <p style="text-align: center">CPO</p>
            </a>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/naufal.png') }});">
                <img src="{{ asset('asset/person_pic/naufal.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Naufal Hilmi Irfandi</h2>
                <p style="text-align: center">CTO</p>
            </a>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/angga.png') }});">
                <img src="{{ asset('asset/person_pic/angga.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Muhammad Erlangga</h2>
                <p style="text-align: center">co-CTO</p>
            </a>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/neesha.png') }});">
                <img src="{{ asset('asset/person_pic/neesha.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Agneesha Bilqis H.</h2>
                <p style="text-align: center">CMO</p>
            </a>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <a class="card-image" style="min-height: 28vh; background-size: 11rem; background-image: url({{ asset('asset/person_pic/zio.png') }});">
                <img src="{{ asset('asset/person_pic/zio.png') }}" alt="Psychopomp" />
            </a>
            <a class="card-description">
                <h2 style="font-size: 1.6rem;line-height: 45px;">Farizio Kautsar Heruzy</h2>
                <p style="text-align: center">CCO</p>
            </a>
        </div>
    </div>
</div>

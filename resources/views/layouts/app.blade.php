<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{ asset('asset/Click_Logo_1.svg') }}">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('cssjs/style.css') }}">
        <title>CLICK</title>
    </head>
    @if (Request::is('comingsoon'))
        <body class="backgroundComingSoon">
    @elseif (Request::is('home'))
        <body class="background">
    @elseif (Request::is('about'))
        <body class="backgroundAboutUs">
    @elseif (Request::is('catering'))
        <body class="backgroundCatering">
    @elseif (Request::is('convection'))
        <body class="backgroundKonveksi">
    @elseif (Request::is('laundry'))
        <body class="backgroundLaundry">
    @elseif (Request::is('logistics'))
        <body class="backgroundLogistics">
    @elseif (Request::is('souvenir'))
        <body class="backgroundSouvenir">
    @else
        <body class="background404">
    @endif

    @include('inc.loader')


    @if(Request::is('catering','convection','laundry','logistics','souvenir','home','about'))
        @include('inc.navbar')
        @yield('content')
        @include('inc.footer')
    @else
        @yield('content')
    @endif
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="{{ asset('cssjs/card.js') }}"></script>
    <script src="{{ asset('cssjs/navbar.js') }}"></script>
    <script src="{{ asset('cssjs/load.js') }}"></script>
    <script src="{{ asset('cssjs/slidehome.js') }}"></script>
    </body>
</html>

@extends('layouts.app')

@section('content')

<div class="container" style="margin-top: 10vh">
    <div class="jumbotron">
        <div class="row" style="margin: 0; height: 30vh;">
            <h1 class="titleComingSoon">Welcome <br>to &emsp;lick</h1>
            <img class="" src="../asset/comingSoon/lift_c.svg" height="380px" style="position: relative; left: -445px; top: 230px" alt="">
            <img class="margin-handComingSoon box one" src="../asset/comingSoon/hand.png" alt="">
        </div>
        <div class="container margin-upComingSoon">
            <div class="jumbotron pt-5">
                <div class="pl-auto comingsoonmessagediv">
                    <h1 class="comingsoonmessage"><span class="font-weight-bold">Oops!</span> Looks like you came here too soon. Follow our <a href="https://www.instagram.com/clickapps.id/" style="color: #C14F16"><span class="font-weight-bold">Instagram</span></a> for future updates!</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('cssjs/card.js') }}"></script>
<script src="{{ asset('cssjs/navbar.js') }}"></script>
<script src="{{ asset('cssjs/slidehome.js') }}"></script>

@endsection

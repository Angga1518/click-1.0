@extends('layouts.app')

@section('content')

<div class="container">
    <div class="pageheader">
        <div class="container">
            <h1 class="aboutusheader" id="aboutUs">About Us</h1>
            <div class="row">
                <div class="col">
                <p><b>CLICK</b> adalah <b><i>on-demand services marketplace</i></b> berbasis aplikasi. 
                    <b>Aplikasi</b> ini membantu dan memudahkan untuk <b>menghubungkan</b> antara <b>penyedia layanan dan konsumen</b>. 
                    Membantu konsumen menemukan penyedia jasa yang mereka sukai dan membantu penyedia jasa untuk memperluas pasar mereka. 
                    <br>
                    <br>
                    Memperkenalkan <b>logo kami: “C”</b> 
                    <br>
                    <b>C</b> terbentuk dari <b>huruf C</b> yang melambangkan <b>CLICK</b> dan <b>cursor berbentuk pesawat kertas</b> 
                    yang berarti CLICK ingin mengajak banyak orang untuk <b>terbang tinggi, mencapai target, dan menuju kebaikan</b>.
                </div>
                <div class="col show-web"></div>
            </div>
        </div>
    </div>
    <div style="height: 200px"></div>
    <div class="col" id="core">
        <h1>Our Core Value</h1>
    </div>
    @include('inc.cardAboutUs')
    <div style="height: 60px"></div>
    <div id="ourTeam" class="col">
        <h1>Our Team</h1>
        <p>Tim CLICK terdiri dari para profesional muda di bidang teknologi, bisnis, ekonomi, dan manajemen yang berkomitmen penuh untuk memberikan dampak positif bagi pengguna CLICK dan juga masyarakat luas.</p>
    </div>
    @include('inc.cardOurTeam')
</div>

@endsection

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="pageheader">
        <h1 class="aboutusheader" id="aboutUs">Laundry</h1>
        <div class="row">
            <div class="col">
            <p>Banyak masyarakat di Indonesia yang menggunakan jasa laundry sebagai
            alternative untuk menghemat waktu, namun sayangnya banyak masyarakat yang sering
            kecewa saat hasil laundr-nya tidak sesuai ekspektasi dan harus mencari-cari laundry lagi
            sehingga membuang waktu. Maka dari itu, CLICK hadir untuk membantu konsumen
            menemukan jasa laundry yang sesuai keinginannya dengan mudah. Selain itu, CLICK
            juga hadir untuk membuka peluang kepada UMKM jasa laundry dalam mendapatkan
            konsumen yang lebih banyak.</p>
            </div>
            <div class="col show-web"></div>
        </div>
    </div>
    <!-- <div style="height: 180px"></div>
    <div class="row">
        <div class="col-sm">
            <h1>Aman</h1>
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, 
            dan lainnya. Sulitnya menemukan dengan kualitas yang sesuai 
            keinginan dan harga yang sesuai budget, para pengguna jasa 
            konveksi sering kali merasa tertipu.</p>
        </div>
        <div class="col-1 show-web"></div>
        <div class="col-sm pic_contain">
            <img src="{{ asset('asset/convection/1.png') }}" alt="">
        </div>
    </div>
    <div style="height: 90px"></div>
    <div class="row">
        <div class="col-sm order-sm-3">
            <h1>Murah</h1>
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, 
            dan lainnya. Sulitnya menemukan dengan kualitas yang sesuai 
            keinginan dan harga yang sesuai budget, para pengguna jasa 
            konveksi sering kali merasa tertipu.</p>
        </div>
        <div class="col-1 show-web order-sm-2"></div>
        <div class="col-sm pic_contain order-sm-1">
            <img src="{{ asset('asset/convection/1.png') }}" alt="">
        </div>
    </div> -->
    <div style="height: 270px"></div>
</div>

@endsection

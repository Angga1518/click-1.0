@extends('layouts.app')

@section('content')

<div class="container">
    <div class="jumbotron">
        <div style="height: 200px"></div>
        <div>
            <h1 class="title">Welcome <br>to <b>CLICK</b></h1>
        </div>
    </div>
    <div class="jumbo-height"></div>
    <div class="container">
        <div class="row" id="aboutUs1">
            <div class="col">
                <h1 class="header">Our Philosophy</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 phil-pic">
                <img src="{{ asset('asset/LogoYellow.svg') }}" height="200px" alt="" style="margin-bottom: 30px" class="mx-auto">
                <!-- <svg xmlns="http://www.w3.org/2000/svg" class="logohome-size" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 547.13 472.71"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="673.08" y1="553.85" x2="70.08" y2="31.85" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ec791c"/><stop offset="1" stop-color="#f49a32"/></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M532.32,60.36a24,24,0,0,0-26.16,5.2L276.35,295.37a24,24,0,0,0,17,41h94.13A181.28,181.28,0,0,1,150.37,396C62.18,348.6,29.15,238.67,76.57,150.49A181.32,181.32,0,0,1,300.3,66.73a27.49,27.49,0,0,0,34-12.69h0a27.55,27.55,0,0,0-14.6-38.8C210.49-26.12,84.9,18.82,28.11,124.43-33.55,239.1,9.65,382.82,124.31,444.48c111.82,60.13,250.46,21,314.82-87l67,67a24,24,0,0,0,41-17v-325A24,24,0,0,0,532.32,60.36ZM499.13,349.59l-54.22-54.22a24,24,0,0,0-17-7H351.26L499.13,140.47Z"/></g></g></svg> -->
            </div>
            <div class="col-md">
                <p class="description">Selamat datang di <b>CLICK</b>! <b>CLICK</b> adalah <b><i>on-demand services marketplace</i></b> berbasis aplikasi. 
                    <b>Aplikasi</b> ini membantu dan memudahkan untuk <b>menghubungkan</b> antara <b>penyedia layanan dan konsumen</b>. 
                    Membantu konsumen menemukan penyedia jasa yang mereka sukai dan membantu penyedia jasa untuk memperluas pasar mereka. 
                    “C” adalah logo kami yang melambangkan <b>CLICK</b> ingin mengajak banyak orang untuk <b>terbang tinggi, mencapai target, 
                    dan menuju kebaikan</b>. Ingin tahu lebih banyak?</p>
                <div id="hoverA">
                    <div class="hoverA mx-auto">
                        <span class="hover hover-1"><a class="description db" href="/about/">Find out more.</a></span>
                    </div>
                </div>
            </div>
        </div>
        @include('inc.slidehome')
    </div>

    <div class="row show-mobile">
        <div class="container">
            <div class="container">
                @include('inc.servicesDesc')
            </div>
        </div>
    </div>
    <div id="service1" class="row descRow xsnap">
        @include('inc.card')
    </div>
    <div  style="height: 150px"></div>
</div>

@endsection

@extends('layouts.app')

@section('content')

<div class="container">
    <div class="pageheader">
        <h1 class="aboutusheader" id="aboutUs">Konveksi</h1>
        <div class="row">
            <div class="col">
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, dll. 
            Sulitnya menemukan dengan kualitas yang sesuai keinginan dan harga 
            yang sesuai budget, para pengguna jasa konveksi sering kali merasa 
            tertipu. Selain itu, UMKM penyedia jasa konveksi juga sulit dalam 
            memperluas pangsa pasar. Oleh karena itu, CLICK akan menjadi solusi 
            untuk menghubungkan pengguna dengan penyedia jasa konveksi yang akan 
            memenuhi ekspektasi.</p>
            </div>
            <div class="col show-web"></div>
        </div>
    </div>
    <!-- <div style="height: 180px"></div>
    <div class="row">
        <div class="col-sm">
            <h1>Aman</h1>
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, 
            dan lainnya. Sulitnya menemukan dengan kualitas yang sesuai 
            keinginan dan harga yang sesuai budget, para pengguna jasa 
            konveksi sering kali merasa tertipu.</p>
        </div>
        <div class="col-1 show-web"></div>
        <div class="col-sm pic_contain">
            <img src="{{ asset('asset/convection/1.png') }}" alt="">
        </div>
    </div>
    <div style="height: 90px"></div>
    <div class="row">
        <div class="col-sm order-sm-3">
            <h1>Murah</h1>
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, 
            dan lainnya. Sulitnya menemukan dengan kualitas yang sesuai 
            keinginan dan harga yang sesuai budget, para pengguna jasa 
            konveksi sering kali merasa tertipu.</p>
        </div>
        <div class="col-1 show-web order-sm-2"></div>
        <div class="col-sm pic_contain order-sm-1">
            <img src="{{ asset('asset/convection/1.png') }}" alt="">
        </div>
    </div> -->
    <div style="height: 290px"></div>
</div>

@endsection

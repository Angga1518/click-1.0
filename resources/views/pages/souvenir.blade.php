@extends('layouts.app')

@section('content')

<div class="container">
    <div class="pageheader">
        <h1 class="aboutusheader" id="aboutUs">Souvenir</h1>
        <div class="row">
            <div class="col">
            <p>Indonesia terdiri dari bareneka ragam suku dan daerah yang tentu saja
            memiliki keuinikannya masing-masing mulai dari makanan khas, baju khas, perhiasan
            khas, dan lain-lain. Namun sayangnya, masih banyak dari mereka yang berbisnis di
            bidang tersebut dan belum terekspos ke publik sehingga kesulitan dalam mencari
            konsumen. Padahal produk yang mereka jual sangat bisa untuk dijadikan oleh-oleh bagi
            para turis domestic maupun mancanegara yang kesulitan mencari oleh-oleh khas daerah
            yang dikunjunginya. Oleh karenanya, CLICK akan membantu untuk mempertemukan
            pencari oleh-oleh khas daerah dengan penjualnya sekaligus memperkenalkan
            keberanekaragaman produk tersebut kepada public</p>
            </div>
            <div class="col show-web"></div>
        </div>
    </div>
    <!-- <div style="height: 180px"></div>
    <div class="row">
        <div class="col-sm">
            <h1>Aman</h1>
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, 
            dan lainnya. Sulitnya menemukan dengan kualitas yang sesuai 
            keinginan dan harga yang sesuai budget, para pengguna jasa 
            konveksi sering kali merasa tertipu.</p>
        </div>
        <div class="col-1 show-web"></div>
        <div class="col-sm pic_contain">
            <img src="{{ asset('asset/convection/1.png') }}" alt="">
        </div>
    </div>
    <div style="height: 90px"></div>
    <div class="row">
        <div class="col-sm order-sm-3">
            <h1>Murah</h1>
            <p>Konveksi sering digunakan untuk membuat seragam, merchandise, 
            dan lainnya. Sulitnya menemukan dengan kualitas yang sesuai 
            keinginan dan harga yang sesuai budget, para pengguna jasa 
            konveksi sering kali merasa tertipu.</p>
        </div>
        <div class="col-1 show-web order-sm-2"></div>
        <div class="col-sm pic_contain order-sm-1">
            <img src="{{ asset('asset/convection/1.png') }}" alt="">
        </div>
    </div> -->
    <div style="height: 210px"></div>
</div>

@endsection

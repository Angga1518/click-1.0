@extends('layouts.app')

@section('content')
<body class="background404">
    <div class = "box-tulisan-404">
        <p class = "tulisan-404"> Looks like you’re lost, kiddo.</p>
    </div>
    <div class = "button-back">
        <span class="hover hover-1"><a class="description db errorButton" href="/">I guess so. I’ll go back</a></span>
    </div>

    <script src="{{ asset('cssjs/card.js') }}"></script>
    <script src="{{ asset('cssjs/slidehome.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
</body>
@endsection
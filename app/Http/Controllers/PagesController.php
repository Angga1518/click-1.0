<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    
    public function goToHomepage(){
        return redirect('home/');
    }
    public function homepage(){
        return view('pages.homepage');
    }
    public function comingsoon(){
        return view('pages.comingsoon');
    }
    public function aboutus(){
        return view('pages.aboutus');
    }
    public function contactus(){
        return view('pages.contactus');
    }
    public function catering(){
        return view('pages.catering');
    }
    public function convection(){
        return view('pages.convection');
    }
    public function laundry(){
        return view('pages.laundry');
    }
    public function logistics(){
        return view('pages.logistics');
    }
    public function souvenir(){
        return view('pages.souvenir');
    }
    // public function services(){

    //     $data = array(
    //         'nama' => 'Muhammad Erlangga',
    //         'umur' => '20 tahun',
    //         'pendidikan' => ['sd', 'smp', 'sma']
    //     );
    //     // return view('pages.services', compact('nama', 'umur'));
    //     return view('pages.services')->with($data) ;
    // }
}

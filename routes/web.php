<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/about/{id}', function ($id) {
//     return 'this is '.$id;
// });

// Route::get('/', 'PagesController@comingsoon');
Route::get('/', 'PagesController@goToHomepage');
Route::get('/home', 'PagesController@homepage');
Route::get('/about', 'PagesController@aboutus');
Route::get('/catering', 'PagesController@catering');
Route::get('/convection', 'PagesController@convection');
Route::get('/laundry', 'PagesController@laundry');
Route::get('/logistics', 'PagesController@logistics');
Route::get('/souvenir', 'PagesController@souvenir');


